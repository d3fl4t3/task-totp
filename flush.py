#!/usr/bin/env python3

from rethinkdb import RethinkDB
r = RethinkDB()

import main
from pprint import pprint

if __name__ == "__main__":
    c = r.connect(*main.DB)
    pprint(r.db_drop('totp').run(c))
