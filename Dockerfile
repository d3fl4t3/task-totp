FROM alpine:3.6
EXPOSE 8080
WORKDIR /usr/src/app
RUN apk add --no-cache python3 python3-dev gcc musl-dev
ADD requirements.txt .
RUN pip3 install --no-cache-dir -r requirements.txt
CMD waitress-serve --host 0.0.0.0 main:app
# CMD [ "python3", "start.py"]
COPY . .
